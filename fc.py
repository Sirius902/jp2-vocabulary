from collections import deque
import csv
import random
from pathlib import Path

def load_flashcards(path):
    with open(path, 'r', encoding='utf8') as f:
        return [tuple(row) for row in csv.reader(f)]

while True:
    user_input = input('What would you like to practice? ')
    fc_path = Path(f'./flashcards/{user_input}.csv')
    if fc_path.exists():
        break
    print(f'Flashcards not found for lesson {user_input}!')

fcs = deque(load_flashcards(str(fc_path)))
random.shuffle(fcs)

while len(fcs) > 0:
    card = fcs.popleft()
    for info in card:
        input(info)
    while True:
        response = input('Did you get it correct? (y/n): ').lower()
        if response in ['y', 'n']:
            break
        print('Invalid input; try again.')
    if response == 'n':
        fcs.append(card)
    print()

print('Finished!')
